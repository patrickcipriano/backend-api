<?php
    /**
    * Tells the browser to allow code from any origin to access
    */
    header("Access-Control-Allow-Origin: *");


    /**
    * Tells browsers whether to expose the response to the frontend JavaScript code
    * when the request's credentials mode (Request.credentials) is include
    */
    header("Access-Control-Allow-Credentials: true");
 


    /**
    * Specifies one or more methods allowed when accessing a resource in response to a preflight request
    */
    header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
 
    /**
    * Used in response to a preflight request which includes the Access-Control-Request-Headers to
    * indicate which HTTP headers can be used during the actual request
    */
    header("Access-Control-Allow-Headers: Content-Type");

    require_once('includes/MysqliDb.php');

    class API {
        public function __construct(){
            $this->db = new MysqliDb('localhost', 'root', '', 'employee');
        }

        /**
        * 
        * HTTP GET Request
        *
        * @param array $payload
        *
        **/

        public function httpGet($payload){
            if(is_array($payload)){
    
                //Displays a success response
                echo "Payload is an array.";
    
                //If the payload is an array, it will execute a query using MysqliDb class 
                //to fetch data into the database.
                //Doesn't check if the array is an associative array yet
                $id = $db->get("employee", $payload);
    
                if($id){
                    //This should be a success response (probably status code)
                    echo "User was created.";
                } else {
                     //This should be a failed response (probably status code)
                    die("Failed Fetch Request. " . $db->getLastError()); 
                }
            } else {
                //Put an error response here
                echo "Payload isn't an array.";
            }
        }

        /**
        * 
        * HTTP POST Request
        *
        * @param array $payload
        *
        **/

        public function httpPost($payload){
            if(is_array($payload)){
    
                //Displays a success response
                echo "Payload is an array.";
    
                //If the payload is an array, it will execute a query using MysqliDb class to insert data 
                //into the database.
                //Doesn't check if the array is an associative array yet
                $id = $db->insert("employee", $payload);
    
                if($id){
                    //This should be a success response (probably status code)
                    echo "User was created.";
                } else {
                     //This should be a failed response (probably status code)
                    die("Failed to Insert Data. " . $db->getLastError()); 
                }
            } else {
                //Put an error response here
                echo "Payload isn't an array.";
            }
        }

        /**
        * 
        * HTTP Put Request
        *
        * @param array $payload
        * @param $id
        *
        **/

        public function httpPut($id, $payload){

            //Checks if the $id is not null or empty
            if(isset($id) && !empty($id)){
                //Code here
            }

            //Checks if the $payload is not empty
            if(!empty($payload)){
                //Code here
            }

            //Checks if the $id is the same as in the $payload
            if($id == $payload["id"]){
                //Code here
            }
        }

        /**
        * 
        * HTTP DELETE Request
        *
        * @param array $payload
        * @param $id
        *
        **/

        public function httpDelete($id, $payload){

            //Checks if the $id is not null or empty
            if(isset($id) && !empty($id)){
                //Code here
            }

            //Checks if the $payload is not empty
            if(!empty($payload)){
                //Code here
            }

            //Checks if the $id is the same as in the $payload
            if($id == $payload["id"]){
                //Code here
            }     
        }
    }

    //Identiier if what type of request
    $request_method = $_SERVER['REQUEST_METHOD'];

    // For GET,POST,PUT & DELETE Request
    if ($request_method === 'GET') {
        $received_data = $_GET;
    } else {
        
        //check if method is PUT or DELETE, and get the ids on URL
        if ($request_method === 'PUT' || $request_method === 'DELETE') {
            $request_uri = $_SERVER['REQUEST_URI'];
            $ids = null;
            $exploded_request_uri = array_values(explode("/", $request_uri));
            $last_index = count($exploded_request_uri) - 1;
            $ids = $exploded_request_uri[$last_index];
        }
    }   

    //payload data
    $received_data = json_decode(file_get_contents('php://input'), true);

    //Creating an instance of the API
    $api = new API;

    switch ($request_method){
        case 'GET':
            $api->httpGet($received_data);
            break;
        case 'POST':
            $api->httpPost($received_data);
            break;
        case 'PUT':
            $api->httpPut($ids, $received_data);
            break;
        case 'DELETE':
            $api->httpDelete($ids, $received_data);
            break;
    }
?>